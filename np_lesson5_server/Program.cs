﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace np_lesson5_server
{
    class Program
    {
        static Socket srvSocket;
        static string ipServer = "0.0.0.0";
        static int portSrv = 12345;
        static void Main(string[] args)
        {
            srvSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            srvSocket.Bind(new IPEndPoint(IPAddress.Parse(ipServer), portSrv));
            EndPoint clientEndPoint = new IPEndPoint(0, 0);
            byte[] buffer = new byte[1500];

            //Получить имя файла
            srvSocket.ReceiveFrom(buffer, ref clientEndPoint);
            string fileName = Encoding.UTF8.GetString(buffer);
            Console.WriteLine("File name" + fileName);
            FileStream outFile = new FileStream(fileName, FileMode.Create);

            //Получить количество посылок
            srvSocket.ReceiveFrom(buffer, ref clientEndPoint);
            int countReceive = BitConverter.ToInt32(buffer,0);
            Console.WriteLine("Count for receive = {0}", countReceive);
            int fullSize = 0;
           

            for (int i = 0; i < countReceive; i++)
            {
                int recSize = srvSocket.ReceiveFrom(buffer, ref clientEndPoint);
                if (recSize <= 0) break;
                Console.Write($"{i}) Receive {recSize} bytes \r");
                outFile.Write(buffer, 0, recSize);
                fullSize += recSize;
            }
            Console.WriteLine($"\nFull size of file = {fullSize}");
            outFile.Flush(true); // Принудительный сброс на диск, новые данные допишутся
            outFile.Close();
            //outFile.Dispose();
        }
    }
}
