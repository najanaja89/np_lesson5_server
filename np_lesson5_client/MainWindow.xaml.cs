﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace np_lesson5_client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Socket client;
        private string fullPath = "";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonSelectClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            fullPath = openFileDialog.FileName;
            textBoxFileName.Text = openFileDialog.SafeFileName;
        }

        private void ButtonSendClick(object sender, RoutedEventArgs e)
        {
            byte[] buffer;
            EndPoint srvEndPoint = new IPEndPoint(0, 0);
            client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //Отправляем имя файла
            client.SendTo(Encoding.UTF8.GetBytes(textBoxFileName.Text), srvEndPoint = new IPEndPoint(IPAddress.Parse(textBoxServerIp.Text), int.Parse(textBoxPortIp.Text)));
            using (var fileStream = new FileStream(fullPath, FileMode.Open))
            {
                int oneBlockize = int.Parse(textBoxOneBlockSize.Text);
                long countSend = fileStream.Length / oneBlockize;
                long remainder = fileStream.Length % oneBlockize;
                buffer = new byte[oneBlockize];
                //Отправляем количество посылок
                int sendSize = client.SendTo(Encoding.UTF8.GetBytes((countSend + 1).ToString()), srvEndPoint = new IPEndPoint(IPAddress.Parse(textBoxServerIp.Text), int.Parse(textBoxPortIp.Text)));
                while (fileStream.Read(buffer, 0, (int)countSend + 1) > 0)
                {
                    client.SendTo(buffer, srvEndPoint = new IPEndPoint(IPAddress.Parse(textBoxServerIp.Text), int.Parse(textBoxPortIp.Text)));
                    Thread.Sleep(20);
                }

                //var str = "Hello ";
                //string ipSrv = "127.0.0.1";
                //int port = 12345;


                //Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                //clientSocket.Bind(new IPEndPoint(IPAddress.Any, 12345));
                //byte[] buf = new byte[64 * 1024];

                //int recSize = client.ReceiveFrom(buf, ref srvEndPoint);
                //textBoxCurrentTime.Text = Encoding.UTF8.GetString(buf, 0, recSize);
            }
        }
    }
}
